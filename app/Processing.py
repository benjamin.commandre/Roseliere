#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of PHYMOBAT 2.0.
# Copyright 2016 Sylvio Laventure (IRSTEA - UMR TETIS)
# 
# PHYMOBAT 2.0 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PHYMOBAT 2.0 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PHYMOBAT 2.0.  If not, see <http://www.gnu.org/licenses/>.

import os
import numpy as np
import configparser 

from app.Toolbox import Toolbox

# Class group image
from app.Archive import Archive

import app.Constantes as Constantes

from collections import defaultdict
from multiprocessing import Process
from multiprocessing.managers import BaseManager, DictProxy

class Processing(object):
	
	"""
		Main processing.
		
		This way is broken down into 2 parts :
			- Image Processing (Search, download and processing)
			- Classification 

		**Main parameters**
		
		@param captor_project: Satellite captor name
		@type captor_project: str

		@param classif_year: Classification year
		@type classif_year: str

		@param nb_avalaible_images: Number download available images
		@type nb_avalaible_images: int

		@param path_folder_dpt: Main folder path
		@type path_folder_dpt: str
		
		@param folder_processing: Processing folder name. By default : 'Traitement'
		@type folder_processing: str
		
		@param path_area: Study area shapefile
		@type path_area: str
		
		**Id information to download on theia platform**
		
		@param user: Connexion Username
		@type user: str

		@param password: Connexion Password
		@type password: str
		
		**Output parameters**
		
		@param output_name_moba: Output classification shapefile 
		@type output_name_moba: str

		@param out_fieldname_carto: Output shapefile field name
		@type out_fieldname_carto: list of str

		@param out_fieldtype_carto: Output shapefile field type
		@type out_fieldtype_carto: list of str (eval ogr pointer)
		
	"""
	
	def __init__(self):
		# List of output raster path
		self.raster_path = defaultdict(dict)
				
	def i_download(self):
		"""
			Interface function to download archives on the website Theia Land. This function extract 
			the number of downloadable image with :func:`Archive.Archive.listing`.
			
			Then, this function download :func:`Archive.Archive.download` and unzip :func:`Archive.Archive.decompress` images
			in the archive folder (**folder_archive**).
		"""

		self.images_folder = "{0}/Images".format(self.path_folder_dpt)

		if not os.path.exists(self.images_folder) :
			os.makedirs(self.images_folder)

		self.check_download = Archive(self.captor_project, self.classif_year, self.path_area, self.images_folder, self.path_area)
		self.nb_avalaible_images = self.check_download.listing()
		self.check_download.download_auto(self.user, self.password, self.proxy)
		self.check_download.decompress()

	def i_img_sat(self):	
		"""
			Interface function to processing satellite images:
			
				1. Clip archive images and modify Archive class to integrate clip image path.
				With :func:`Toolbox.clip_raster` in ``Toolbox`` module.
			
				2. Search cloud's percentage :func:`RasterSat_by_date.RasterSat_by_date.pourc_cloud`, 
				select image and compute ndvi index :func:`RasterSat_by_date.RasterSat_by_date.calcul_ndvi`.
				If cloud's percentage is greater than 40%, then not select and compute ndvi index.
			
				3. Compute temporal stats on ndvi index [min, max, std, min-max]. With :func:`Toolbox.calc_serie_stats` 
				in ``Toolbox`` module.
				
				4. Create stats ndvi raster and stats cloud raster.
				
				>>> import RasterSat_by_date
				>>> stats_test = RasterSat_by_date(class_archive, big_folder, one_date)
				>>> stats_test.complete_raster(stats_test.create_raster(in_raster, stats_data, in_ds), stats_data)
		"""
		pass		
		# current_list = Toolbox()
		# current_list.vect = self.path_area

		# # Map projection of the image and the cloud mask
		# for clip_index, clip in enumerate(self.check_download.list_img):
			
		# 	current_list.image = clip[3]
			
		# 	current_list.check_proj() # Check if projection is RFG93 
		# 	self.check_download.list_img[clip_index][3] = current_list.clip_raster() # Multispectral images
			
		# 	current_list.image = clip[4]
		# 	current_list.check_proj() # Check if projection is RFG93 

		# 	self.check_download.list_img[clip_index][4] = current_list.clip_raster() # Cloud images

		# # Images pre-processing
		# spectral_out = []

		# check_L8 = RasterSat_by_date(self.check_download, self.folder_processing)

		# for date in self.check_download.single_date:

		# 	check_L8.mosaic_by_date(date)
			 
		# 	# Search cloud's percentage, select image and compute ndvi index 
		# 	if check_L8.pourc_cloud() < Constantes.CLOUD_THRESHOLD :
		# 		tmp = date
		# 		check_L8.calcul_ndvi()
		# 		tmp.extend(check_L8.cloudiness_pourcentage)
		# 		spectral_out.append(tmp)

		# # Compute temporal stats on ndvi index [min, max, std, min-max]
		# spectral_trans = np.transpose(np.array(spectral_out, dtype=object))

		# del spectral_out 

		# # stats_name = ['Min', 'Date', 'Max', 'Std', 'MaxMin']
		# stats_name = ['Min', 'Max']
		# path_stat = current_list.calc_serie_stats(spectral_trans, stats_name, self.folder_processing)

		# for idx, i in enumerate(stats_name):
		# 	self.out_ndvistats_folder_tab[i] = path_stat[idx]

		# check_L8.logger.close()
		# current_list.logger.close()

	def i_vhrs(self):
		"""
			Interface function to processing VHRS images. 
			It create two OTB texture images : 
				func: `Vhrs.Vhrs` : SFS Texture and Haralick Texture
		"""

		############################## Create texture image ##############################
		
		# Clip orthography image 

		pass

		# current_path_ortho = Toolbox()

		# current_path_ortho.image = self.path_ortho
		# current_path_ortho.vect = self.path_area
		# current_path_ortho.output = "{0}/MosaiqueOrtho/Clip_{1}".format(self.folder_processing, os.path.basename(self.path_ortho))
		
		# self.path_ortho = current_path_ortho.output
		# path_ortho = current_path_ortho.clip_raster()
		
		# texture_irc = Vhrs(path_ortho, self.mp)
		# self.out_ndvistats_folder_tab['sfs']      = texture_irc.out_sfs
		# self.out_ndvistats_folder_tab['haralick'] = texture_irc.out_haralick

		# current_path_ortho.logger.close()
		# texture_irc.logger.close()
		
	def i_images_processing(self): 
		"""
			Interface function to launch processing VHRS images : func:`i_vhrs` 
			and satellite images :func:`i_img_sat` in multi-processing.
		"""

		pass

		# mgr = BaseManager()
		# mgr.register('defaultdict', defaultdict, DictProxy)
		# mgr.start()

		# self.out_ndvistats_folder_tab = mgr.defaultdict(list)		
		
		# p_img_sat = Process(target=self.i_img_sat)
		# p_vhrs = Process(target=self.i_vhrs)

		# self.logger.debug("Multiprocessing : {0}".format(self.mp))
		
		# if self.mp == Constantes.MULTIPROCESSING_DISABLE:
		# 	p_img_sat.start()
		# 	p_img_sat.join()
		# 	p_vhrs.start()
		# 	p_vhrs.join()	
		# else :
		# 	p_img_sat.start()
		# 	p_vhrs.start()
		# 	p_img_sat.join()
		# 	p_vhrs.join()

		# ###################################################################

		# self.raster_path["Min"]["PATH"] = self.out_ndvistats_folder_tab['Min']
		# self.raster_path["Min"]["BAND"] = 1
		
		# for i in range(1,7) :
		# 	self.raster_path["SFS_{0}".format(i)]["PATH"] = self.out_ndvistats_folder_tab['sfs']
		# 	self.raster_path["SFS_{0}".format(i)]["BAND"] = i

		# for i in range(1,9) :
		# 	self.raster_path["HARALICK_{0}".format(i)]["PATH"] = self.out_ndvistats_folder_tab['haralick']
		# 	self.raster_path["HARALICK_{0}".format(i)]["BAND"] = i
					
		# self.raster_path["MAX"]["PATH"] = self.out_ndvistats_folder_tab['Max']
		# self.raster_path["MAX"]["BAND"] = 1
		
		# self.logger.info("End of images processing !")
