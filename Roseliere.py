#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 
# Copyright 2016 Sylvio Laventure (IRSTEA - UMR TETIS)
#
# PHYMOBAT 3.0 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# PHYMOBAT 3.0 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with PHYMOBAT 3.0.  If not, see <http://www.gnu.org/licenses/>.

"""
	Main interface

	__name__ = "Roseliere 0.1"

	__license__ = "GPL"

	__version__ = "0.1"

	__author__ = "COMMANDRÉ Benjamin - UMR TETIS / IRSTEA"

	__date__ = "Septembre 2018"
"""

import os, sys, time

from _collections import defaultdict

from osgeo import ogr

import configparser 
from app.Processing import Processing
import app.Constantes as Constantes
import app.Outils as Outils

class Roseliere(Processing):
	"""
		Interface main class. It makes to link ``ui_PHYMOBAT_tab`` and ``Processing``.
	"""
	
	def __init__(self, parent=None):
		super(Processing, self).__init__()
		Processing.__init__(self)

		self.logger = Outils.Log("log", "Roseliere")
					
		self.get_variable()
						
	def get_variable(self):		
		"""
			Add a all system value like :
		
			- Main folder path by line edit
			- Satellite captor name by combo box
			- Classification year by line edit
			- Study area shapefile path by line edit
			- Connexion username and password by line edit
			- VHRS image path by line edit
			- MNT image path by line edit
			- Segmentation shapefile path path by line edit
			- Output classification shapefile path by line edit
			- Output shapefile field name by line edit and field type by combo box
		"""

		configfile = configparser.ConfigParser()
		configfile.read("config.ini")

		# Main folder path by line edit.
		self.path_folder_dpt = "{0}/Traitement".format(configfile["sortie"]["chemin"])
		
		# Satellite captor name by combo box
		self.captor_project = "{0}".format(configfile["satellite"]["capteur"])
		
		# Classification year by line edit
		self.classif_year = "{0}".format(configfile["emprise"]["annee_debut"])
		
		# Study area shapefile path by line edit
		self.path_area = "{0}".format(configfile["emprise"]["chemin"])
		
		# Connexion username and password by line edit
		self.user = "{0}".format(configfile["theia"]["identifiant"])
		self.password = "{0}".format(configfile["theia"]["mdp"])
		self.proxy = "{0}".format(configfile["theia"]["proxy"])

		# Output shapefile field name by line edit and field type by combo box
		self.output_file = "{0}".format(configfile["sortie"]["chemin"])
	
	def run(self):
		"""
			Function to launch the processing. This function take account :
			
			- The ``Multi-processing`` check box if the processing has launched with multi process.
				By default, this is checked. It need a computer with minimum 12Go memory.
			- Append a few system value with :func:`get_variable`.
			- There are 3 principal check boxes :
				- to get number download available images
				- for downloading and processing on theia platform
				- to compute optimal threshold.
				- to compute slope raster
				- for classification processing.
		"""
		
		# Start the processus
		startTime = time.time()
		
		# # Look at if the the images has been already downloaded, download them otherwise
		self.i_download()
			
		# # function to launch the image processing	
		# self.i_images_processing()

		# # Classification processing 
		# self.i_classifier()
		
		# Initialize variable without to close and launch again the application
		Processing.__init__(self)
		
		# End of the processus
		endTime = time.time()
		self.logger.info('Outputted to File in {0} secondes'.format(endTime - startTime))
		nb_day_processing = int(time.strftime('%d', time.gmtime(endTime - startTime))) - 1
		self.logger.info("That is {0} day(s) {1}".format(nb_day_processing, time.strftime('%Hh %Mmin%S', time.gmtime(endTime - startTime))))
		

		
if __name__ == "__main__":
	
	myapp = Roseliere()		
	sys.exit(myapp.run())
